FROM ubuntu
MAINTAINER geodia <georgios.diapoulis@chalmers.se>
ARG user_id=1000
# RUN usermod -u $user_id ubuntu

ENV LANG C.UTF-8
ENV USER icli
ENV HOME /home/$USER

RUN mkdir -p $HOME
RUN groupadd -r $USER && useradd -r -g $USER $USER

WORKDIR $HOME

# --- update system
RUN \
    apt-get -qq update && \
    apt-get -y upgrade && \
    apt-get install -y curl zip unzip vim wget git grep poppler-utils python3 python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN  \
    pip3 install requests seaborn matplotlib scikit-learn gensim nltk

RUN git clone https://gitlab.com/diapoulis/icli2021-draft.git

WORKDIR $HOME/icli2021-draft

RUN bash icli.sh

CMD ["/bin/bash"]
